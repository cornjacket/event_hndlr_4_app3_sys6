package main

import (
	"net/http"
)

// GET /err?msg=
// shows the error message page
func err(writer http.ResponseWriter, request *http.Request) {
	vals := request.URL.Query()
	generateHTML(writer, vals.Get("msg"), "layout", "public.navbar", "error")
}

func index(writer http.ResponseWriter, request *http.Request) {
	generateHTML(writer, nil, "layout", "public.navbar", "public.index")
}
