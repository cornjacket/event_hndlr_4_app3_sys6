package expectedstateb

import (
  "strconv"
)

type ExpectedStateB struct {
  value int
  updated bool
}

func New() *ExpectedStateB {
  return &ExpectedStateB{}
}

func (x *ExpectedStateB) IsUpdated() bool {
  return x.updated
}

func (x *ExpectedStateB) Get() int {
  return x.value
}

func (x *ExpectedStateB) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *ExpectedStateB) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *ExpectedStateB) String() string {
  return "ExpectedStateB"
}

func (x *ExpectedStateB) Type() string {
  return "int"
}

func (x *ExpectedStateB) Modifiers() string {
  return "not null"
}

func (x *ExpectedStateB) QueryValue() string {
  return strconv.Itoa(x.value)
}

