package comapi

import (
  "strconv"
)

type ComApi struct {
  value int
  updated bool
}

func New() *ComApi {
  return &ComApi{}
}

func (x *ComApi) IsUpdated() bool {
  return x.updated
}

func (x *ComApi) Get() int {
  return x.value
}

func (x *ComApi) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *ComApi) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *ComApi) String() string {
  return "ComApi"
}

func (x *ComApi) Type() string {
  return "int"
}

func (x *ComApi) Modifiers() string {
  return "not null"
}

func (x *ComApi) QueryValue() string {
  return strconv.Itoa(x.value)
}

