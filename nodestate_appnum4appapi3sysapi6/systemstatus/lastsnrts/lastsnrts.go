package lastsnrts

import (
  "strconv"
)

type LastSnrTs struct {
  value int64
  updated bool
}

func New() *LastSnrTs {
  return &LastSnrTs{}
}

func (x *LastSnrTs) IsUpdated() bool {
  return x.updated
}

func (x *LastSnrTs) Get() int64 {
  return x.value
}

func (x *LastSnrTs) Set(value int64) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *LastSnrTs) Load(value int64) {
  x.value = value
  x.updated = false
}

func (x *LastSnrTs) String() string {
  return "LastSnrTs"
}

func (x *LastSnrTs) Type() string {
  return "bigint"
}

func (x *LastSnrTs) Modifiers() string {
  return "not null"
}

func (x *LastSnrTs) QueryValue() string {
  return strconv.FormatInt(x.value, 10)
}

