package lastrssits

import (
  "strconv"
)

type LastRssiTs struct {
  value int64
  updated bool
}

func New() *LastRssiTs {
  return &LastRssiTs{}
}

func (x *LastRssiTs) IsUpdated() bool {
  return x.updated
}

func (x *LastRssiTs) Get() int64 {
  return x.value
}

func (x *LastRssiTs) Set(value int64) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *LastRssiTs) Load(value int64) {
  x.value = value
  x.updated = false
}

func (x *LastRssiTs) String() string {
  return "LastRssiTs"
}

func (x *LastRssiTs) Type() string {
  return "bigint"
}

func (x *LastRssiTs) Modifiers() string {
  return "not null"
}

func (x *LastRssiTs) QueryValue() string {
  return strconv.FormatInt(x.value, 10)
}

