package lastpktrcvdts

import (
  "strconv"
)

type LastPktRcvdTs struct {
  value int64
  updated bool
}

func New() *LastPktRcvdTs {
  return &LastPktRcvdTs{}
}

func (x *LastPktRcvdTs) IsUpdated() bool {
  return x.updated
}

func (x *LastPktRcvdTs) Get() int64 {
  return x.value
}

func (x *LastPktRcvdTs) Set(value int64) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *LastPktRcvdTs) Load(value int64) {
  x.value = value
  x.updated = false
}

func (x *LastPktRcvdTs) String() string {
  return "LastPktRcvdTs"
}

func (x *LastPktRcvdTs) Type() string {
  return "bigint"
}

func (x *LastPktRcvdTs) Modifiers() string {
  return "not null"
}

func (x *LastPktRcvdTs) QueryValue() string {
  return strconv.FormatInt(x.value, 10)
}

