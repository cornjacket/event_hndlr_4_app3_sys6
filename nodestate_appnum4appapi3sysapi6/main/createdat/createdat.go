package createdat

import (
  "strconv"
  "time"
)

type CreatedAt struct {
  value time.Time
  updated bool
}

func New() *CreatedAt {
  return &CreatedAt{}
}

func (x *CreatedAt) IsUpdated() bool {
  return x.updated
}

func (x *CreatedAt) Get() time.Time {
  return x.value
}

func (x *CreatedAt) Set(value time.Time) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *CreatedAt) Load(value time.Time) {
  x.value = value
  x.updated = false
}

func (x *CreatedAt) String() string {
  return "CreatedAt"
}

func (x *CreatedAt) Type() string {
  return "datetime"
}

func (x *CreatedAt) Modifiers() string {
  return "not null default current_timestamp"
}

func (x *CreatedAt) QueryValue() string {
  return strconv.FormatInt(x.value.UnixNano(), 10)
}

