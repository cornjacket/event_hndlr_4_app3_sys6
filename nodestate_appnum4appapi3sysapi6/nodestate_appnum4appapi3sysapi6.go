package nodestate_appnum4appapi3sysapi6

import (
  // Standard library packets
  "fmt"
  "os"
  "strconv"
  "database/sql"
  _ "github.com/go-sql-driver/mysql"
  "time"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/main/eui"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/main/validonboard"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/main/createdat"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/main/updatedat"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/systemstatus/appnum"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/systemstatus/appapi"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/systemstatus/sysapi"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/systemstatus/appfwversion"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/systemstatus/sysfwversion"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/systemstatus/comapi"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/systemstatus/comfwversion"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/systemstatus/lastpktrcvd"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/systemstatus/lastpktrcvdts"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/systemstatus/lastvoltagercvd"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/systemstatus/lastvoltagercvdts"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/systemstatus/lastresetcause"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/systemstatus/lastresetcausets"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/systemstatus/lastrssi"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/systemstatus/lastrssits"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/systemstatus/lastsnr"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/systemstatus/lastsnrts"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/systemcontrol/globaldownstreamenable"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/systemcontrol/systemdownstreamenable"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/systemcontrol/appdownstreamenable"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/appstatus/actualstatea"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/appstatus/actualstateb"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/appcontrol/expectedstatea"
  "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6/appcontrol/expectedstateb"
)

type Row struct {
  Eui	string	`json:"eui"`
  ValidOnboard	int	`json:"validonboard"`
  CreatedAt	time.Time	`json:"createdat"`
  UpdatedAt	time.Time	`json:"updatedat"`
  AppNum	int	`json:"appnum"`
  AppApi	int	`json:"appapi"`
  SysApi	int	`json:"sysapi"`
  AppFwVersion	int	`json:"appfwversion"`
  SysFwVersion	int	`json:"sysfwversion"`
  ComApi	int	`json:"comapi"`
  ComFwVersion	int	`json:"comfwversion"`
  LastPktRcvd	string	`json:"lastpktrcvd"`
  LastPktRcvdTs	int64	`json:"lastpktrcvdts"`
  LastVoltageRcvd	int	`json:"lastvoltagercvd"`
  LastVoltageRcvdTs	int64	`json:"lastvoltagercvdts"`
  LastResetCause	int	`json:"lastresetcause"`
  LastResetCauseTs	int64	`json:"lastresetcausets"`
  LastRssi	int	`json:"lastrssi"`
  LastRssiTs	int64	`json:"lastrssits"`
  LastSnr	int	`json:"lastsnr"`
  LastSnrTs	int64	`json:"lastsnrts"`
  GlobalDownstreamEnable	int	`json:"globaldownstreamenable"`
  SystemDownstreamEnable	int	`json:"systemdownstreamenable"`
  AppDownstreamEnable	int	`json:"appdownstreamenable"`
  ActualStateA	int	`json:"actualstatea"`
  ActualStateB	int	`json:"actualstateb"`
  ExpectedStateA	int	`json:"expectedstatea"`
  ExpectedStateB	int	`json:"expectedstateb"`
}

type PrevRow struct {
  Eui	string	`json:"eui"`
  ValidOnboard	int	`json:"validonboard"`
  CreatedAt	time.Time	`json:"createdat"`
  UpdatedAt	time.Time	`json:"updatedat"`
  AppNum	int	`json:"appnum"`
  AppApi	int	`json:"appapi"`
  SysApi	int	`json:"sysapi"`
  AppFwVersion	int	`json:"appfwversion"`
  SysFwVersion	int	`json:"sysfwversion"`
  ComApi	int	`json:"comapi"`
  ComFwVersion	int	`json:"comfwversion"`
  LastPktRcvd	string	`json:"lastpktrcvd"`
  LastPktRcvdTs	int64	`json:"lastpktrcvdts"`
  LastVoltageRcvd	int	`json:"lastvoltagercvd"`
  LastVoltageRcvdTs	int64	`json:"lastvoltagercvdts"`
  LastResetCause	int	`json:"lastresetcause"`
  LastResetCauseTs	int64	`json:"lastresetcausets"`
  LastRssi	int	`json:"lastrssi"`
  LastRssiTs	int64	`json:"lastrssits"`
  LastSnr	int	`json:"lastsnr"`
  LastSnrTs	int64	`json:"lastsnrts"`
  GlobalDownstreamEnable	int	`json:"globaldownstreamenable"`
  SystemDownstreamEnable	int	`json:"systemdownstreamenable"`
  AppDownstreamEnable	int	`json:"appdownstreamenable"`
  ActualStateA	int	`json:"actualstatea"`
  ActualStateB	int	`json:"actualstateb"`
  ExpectedStateA	int	`json:"expectedstatea"`
  ExpectedStateB	int	`json:"expectedstateb"`
}

var db_conn *sql.DB

func Init(db *sql.DB) {
 fmt.Printf("NodeState_Appnum4Appapi3Sysapi6.Init() invoked\n")
  if db != nil {
    db_conn = db
  } else {
   fmt.Printf("NodeState_Appnum4Appapi3Sysapi6.Init() db = nil. Exiting...\n")
    os.Exit(0)
  }
  nodestate_appnum4appapi3sysapi6 := New()
  currentTable := nodestate_appnum4appapi3sysapi6.GetCurrentTableName()
  fmt.Printf("currentTable: %s, CreateTableName: %s\n", currentTable, nodestate_appnum4appapi3sysapi6.CreateTableString())
  // Create table if it doesn't already exist
  err := nodestate_appnum4appapi3sysapi6.CreateTable()
  if err != nil {
    fmt.Printf("nodestate_appnum4appapi3sysapi6.CreateTable() failed. Assuming already exists.\n")
  } else {
    fmt.Printf("nodestate_appnum4appapi3sysapi6.CreateTable() succeeded!!\n")
  }
}

type NodeState_Appnum4Appapi3Sysapi6 struct {
  db           *sql.DB
  tableName    string
  prevVersion  int
  version      int
  constraints  string
  Eui	*eui.Eui
  ValidOnboard	*validonboard.ValidOnboard
  CreatedAt	*createdat.CreatedAt
  UpdatedAt	*updatedat.UpdatedAt
  AppNum	*appnum.AppNum
  AppApi	*appapi.AppApi
  SysApi	*sysapi.SysApi
  AppFwVersion	*appfwversion.AppFwVersion
  SysFwVersion	*sysfwversion.SysFwVersion
  ComApi	*comapi.ComApi
  ComFwVersion	*comfwversion.ComFwVersion
  LastPktRcvd	*lastpktrcvd.LastPktRcvd
  LastPktRcvdTs	*lastpktrcvdts.LastPktRcvdTs
  LastVoltageRcvd	*lastvoltagercvd.LastVoltageRcvd
  LastVoltageRcvdTs	*lastvoltagercvdts.LastVoltageRcvdTs
  LastResetCause	*lastresetcause.LastResetCause
  LastResetCauseTs	*lastresetcausets.LastResetCauseTs
  LastRssi	*lastrssi.LastRssi
  LastRssiTs	*lastrssits.LastRssiTs
  LastSnr	*lastsnr.LastSnr
  LastSnrTs	*lastsnrts.LastSnrTs
  GlobalDownstreamEnable	*globaldownstreamenable.GlobalDownstreamEnable
  SystemDownstreamEnable	*systemdownstreamenable.SystemDownstreamEnable
  AppDownstreamEnable	*appdownstreamenable.AppDownstreamEnable
  ActualStateA	*actualstatea.ActualStateA
  ActualStateB	*actualstateb.ActualStateB
  ExpectedStateA	*expectedstatea.ExpectedStateA
  ExpectedStateB	*expectedstateb.ExpectedStateB
}

func New() *NodeState_Appnum4Appapi3Sysapi6 {
  x := &NodeState_Appnum4Appapi3Sysapi6{}
  x.db = db_conn
  x.tableName = "nodestate_appnum4appapi3sysapi6"
  x.prevVersion = 1
  x.version = 2
  x.Eui=	eui.New()
  x.ValidOnboard=	validonboard.New()
  x.CreatedAt=	createdat.New()
  x.UpdatedAt=	updatedat.New()
  x.AppNum=	appnum.New()
  x.AppApi=	appapi.New()
  x.SysApi=	sysapi.New()
  x.AppFwVersion=	appfwversion.New()
  x.SysFwVersion=	sysfwversion.New()
  x.ComApi=	comapi.New()
  x.ComFwVersion=	comfwversion.New()
  x.LastPktRcvd=	lastpktrcvd.New()
  x.LastPktRcvdTs=	lastpktrcvdts.New()
  x.LastVoltageRcvd=	lastvoltagercvd.New()
  x.LastVoltageRcvdTs=	lastvoltagercvdts.New()
  x.LastResetCause=	lastresetcause.New()
  x.LastResetCauseTs=	lastresetcausets.New()
  x.LastRssi=	lastrssi.New()
  x.LastRssiTs=	lastrssits.New()
  x.LastSnr=	lastsnr.New()
  x.LastSnrTs=	lastsnrts.New()
  x.GlobalDownstreamEnable=	globaldownstreamenable.New()
  x.SystemDownstreamEnable=	systemdownstreamenable.New()
  x.AppDownstreamEnable=	appdownstreamenable.New()
  x.ActualStateA=	actualstatea.New()
  x.ActualStateB=	actualstateb.New()
  x.ExpectedStateA=	expectedstatea.New()
  x.ExpectedStateB=	expectedstateb.New()
  x.constraints = "constraint pk_example primary key (eui)"
  return x
}

var prevTableExists bool = false // TODO: Create Mechanism to auto detect if prevTable exists

func PrevTableExists() bool {
  return prevTableExists;
}

func (x *NodeState_Appnum4Appapi3Sysapi6) GetByEui (eui string) (error, bool) {
  fmt.Printf("table.GetByEui(): Entrance.\n")
  x.Eui.Load(eui)  // required: all rows must have a valid_onboard field that defaults to false, used to indicate if a field was created properly.
  // getByEui should select/search for entry in current version table
  err, rcvd := x.SelectFromCurrent(eui)
  // if entryfound then process_normally
  if err == nil && rcvd == true {
    fmt.Printf("table.GetByEui(): Returning nil, no error, rcvd one entry..\n")
    return nil, true
  }
  // else // no entry found
  fmt.Printf("table.GetByEui(): row DNE in current table.\n")
  if PrevTableExists() {
    fmt.Printf("table.GetByEui(): checking prev table\n")
    err, rcvd = x.SelectFromPrev(eui)
    entryFound := (err == nil && rcvd == true)
    if entryFound {
//       copy over parameters from old row to new row, along with new row's new default fields, with valid_onboard set to true
      x.HandleMigrationOrRollback()
      err = x.insert()
      if err != nil {
        fmt.Printf("table.GetByEui(): x.insert failed.\n")
        return err, rcvd
      }
      fmt.Printf("table.GetByEui(): x.insert succeeded.\n")
      err = x.DeleteByEui(x.GetPrevTableName())
      if err != nil {
        fmt.Printf("table.GetByEui(): x.delete %s failed from PrevTable.\n")
        return err, rcvd
      }
    } else {
//       create default row with valid_onboard = false -- How to do this?
      err = x.insert() // later we can just return this i think
      if err != nil {
        fmt.Printf("table.GetByEui(): x.insert failed.\n")
        return err, rcvd
      }
//     create default row with valid_onboard = false
    }
  } else { // later we can just return this i think
    err = x.insert()
    if err != nil {
      fmt.Printf("table.GetByEui(): x.insert failed.\n")
      return err, rcvd
    }
  }
  return err, rcvd
}

func (x *NodeState_Appnum4Appapi3Sysapi6) HandleMigrationOrRollback() {
// currently doing nothing but this is where the migration or rollback logic should happen
// this should be generated code based on whether migration or rollback
fmt.Printf("table.HandleColumnsForMigrationOrRollback(): \n")
}

func (x *NodeState_Appnum4Appapi3Sysapi6) CreateTable() error {
  _, err := x.db.Exec(x.CreateTableString())
  return err
}

func (x *NodeState_Appnum4Appapi3Sysapi6) DeleteByEui(tableName string) error {
  _, err := x.db.Exec(x.DeleteString(tableName))
  return err
}

func (x *NodeState_Appnum4Appapi3Sysapi6) Insert(row *Row) error {
  x.Eui.Load(row.Eui)
  x.ValidOnboard.Load(row.ValidOnboard)
  x.AppNum.Load(row.AppNum)
  x.AppApi.Load(row.AppApi)
  x.SysApi.Load(row.SysApi)
  x.AppFwVersion.Load(row.AppFwVersion)
  x.SysFwVersion.Load(row.SysFwVersion)
  x.ComApi.Load(row.ComApi)
  x.ComFwVersion.Load(row.ComFwVersion)
  x.LastPktRcvd.Load(row.LastPktRcvd)
  x.LastPktRcvdTs.Load(row.LastPktRcvdTs)
  x.LastVoltageRcvd.Load(row.LastVoltageRcvd)
  x.LastVoltageRcvdTs.Load(row.LastVoltageRcvdTs)
  x.LastResetCause.Load(row.LastResetCause)
  x.LastResetCauseTs.Load(row.LastResetCauseTs)
  x.LastRssi.Load(row.LastRssi)
  x.LastRssiTs.Load(row.LastRssiTs)
  x.LastSnr.Load(row.LastSnr)
  x.LastSnrTs.Load(row.LastSnrTs)
  x.GlobalDownstreamEnable.Load(row.GlobalDownstreamEnable)
  x.SystemDownstreamEnable.Load(row.SystemDownstreamEnable)
  x.AppDownstreamEnable.Load(row.AppDownstreamEnable)
  x.ActualStateA.Load(row.ActualStateA)
  x.ActualStateB.Load(row.ActualStateB)
  x.ExpectedStateA.Load(row.ExpectedStateA)
  x.ExpectedStateB.Load(row.ExpectedStateB)
  return x.insert()
}

func (x *NodeState_Appnum4Appapi3Sysapi6) insert() error {
  insertString := x.InsertString()
  fmt.Printf("table.InsertString(): %s\n", insertString)
  _, err := x.db.Exec(insertString,
   x.Eui.Get(),
   x.ValidOnboard.Get(),
   x.AppNum.Get(),
   x.AppApi.Get(),
   x.SysApi.Get(),
   x.AppFwVersion.Get(),
   x.SysFwVersion.Get(),
   x.ComApi.Get(),
   x.ComFwVersion.Get(),
   x.LastPktRcvd.Get(),
   x.LastPktRcvdTs.Get(),
   x.LastVoltageRcvd.Get(),
   x.LastVoltageRcvdTs.Get(),
   x.LastResetCause.Get(),
   x.LastResetCauseTs.Get(),
   x.LastRssi.Get(),
   x.LastRssiTs.Get(),
   x.LastSnr.Get(),
   x.LastSnrTs.Get(),
   x.GlobalDownstreamEnable.Get(),
   x.SystemDownstreamEnable.Get(),
   x.AppDownstreamEnable.Get(),
   x.ActualStateA.Get(),
   x.ActualStateB.Get(),
   x.ExpectedStateA.Get(),
   x.ExpectedStateB.Get(),
  )
  if err != nil {
    return err
  }
  return nil
}

// updated indicates whether any fields have changed and thus an update to the db was attempted 
func (x *NodeState_Appnum4Appapi3Sysapi6) Update() (anyFieldModified bool, err error) {
  updateString, anyFieldModified := x.UpdateString()
  fmt.Printf("table.UpdateString(): %s\n", updateString)

  if anyFieldModified {
    _, err = x.db.Exec(updateString,
      x.ValidOnboard,
      x.AppNum,
      x.AppApi,
      x.SysApi,
      x.AppFwVersion,
      x.SysFwVersion,
      x.ComApi,
      x.ComFwVersion,
      x.LastPktRcvd,
      x.LastPktRcvdTs,
      x.LastVoltageRcvd,
      x.LastVoltageRcvdTs,
      x.LastResetCause,
      x.LastResetCauseTs,
      x.LastRssi,
      x.LastRssiTs,
      x.LastSnr,
      x.LastSnrTs,
      x.GlobalDownstreamEnable,
      x.SystemDownstreamEnable,
      x.AppDownstreamEnable,
      x.ActualStateA,
      x.ActualStateB,
      x.ExpectedStateA,
      x.ExpectedStateB,
    )
  }
  if err != nil {
    return anyFieldModified, err
  }
  return anyFieldModified, nil
}

func (x *NodeState_Appnum4Appapi3Sysapi6) SelectFromCurrent(eui string) (error, bool) { // later this will be SelectByEui(eui string)
  //d.Id = id
  x.Eui.Load(eui)
  tableName := x.GetCurrentTableName()
  selectString := x.SelectString(tableName) // we could pass eui into select for a nicer abstraction
  fmt.Printf("table.SelectString(): %s\n", selectString)
  results, err := x.db.Query(selectString)
  if err != nil {
    return err, false
  }
  defer results.Close()

  for results.Next() {
    var row Row
//err = results.Scan(&row.Id, &row.Eui, &row.SysApi, &row.AppApi)
    err = results.Scan( &row.Eui, &row.ValidOnboard, &row.CreatedAt, &row.UpdatedAt, &row.AppNum, &row.AppApi, &row.SysApi, &row.AppFwVersion, &row.SysFwVersion, &row.ComApi, &row.ComFwVersion, &row.LastPktRcvd, &row.LastPktRcvdTs, &row.LastVoltageRcvd, &row.LastVoltageRcvdTs, &row.LastResetCause, &row.LastResetCauseTs, &row.LastRssi, &row.LastRssiTs, &row.LastSnr, &row.LastSnrTs, &row.GlobalDownstreamEnable, &row.SystemDownstreamEnable, &row.AppDownstreamEnable, &row.ActualStateA, &row.ActualStateB, &row.ExpectedStateA, &row.ExpectedStateB,)
  if err != nil {
  return err, false
  }
 x.Eui.Load(row.Eui)
 x.ValidOnboard.Load(row.ValidOnboard)
 x.CreatedAt.Load(row.CreatedAt)
 x.UpdatedAt.Load(row.UpdatedAt)
 x.AppNum.Load(row.AppNum)
 x.AppApi.Load(row.AppApi)
 x.SysApi.Load(row.SysApi)
 x.AppFwVersion.Load(row.AppFwVersion)
 x.SysFwVersion.Load(row.SysFwVersion)
 x.ComApi.Load(row.ComApi)
 x.ComFwVersion.Load(row.ComFwVersion)
 x.LastPktRcvd.Load(row.LastPktRcvd)
 x.LastPktRcvdTs.Load(row.LastPktRcvdTs)
 x.LastVoltageRcvd.Load(row.LastVoltageRcvd)
 x.LastVoltageRcvdTs.Load(row.LastVoltageRcvdTs)
 x.LastResetCause.Load(row.LastResetCause)
 x.LastResetCauseTs.Load(row.LastResetCauseTs)
 x.LastRssi.Load(row.LastRssi)
 x.LastRssiTs.Load(row.LastRssiTs)
 x.LastSnr.Load(row.LastSnr)
 x.LastSnrTs.Load(row.LastSnrTs)
 x.GlobalDownstreamEnable.Load(row.GlobalDownstreamEnable)
 x.SystemDownstreamEnable.Load(row.SystemDownstreamEnable)
 x.AppDownstreamEnable.Load(row.AppDownstreamEnable)
 x.ActualStateA.Load(row.ActualStateA)
 x.ActualStateB.Load(row.ActualStateB)
 x.ExpectedStateA.Load(row.ExpectedStateA)
 x.ExpectedStateB.Load(row.ExpectedStateB)
    return nil, true
  }
  return nil, false
}

func (x *NodeState_Appnum4Appapi3Sysapi6) GetPrevTableName() string {
  return tableName(x.tableName, x.prevVersion)
}

//func (x *NodeState_Appnum4Appapi3Sysapi6) Select(id int64) error { // later this will be SelectByEui(eui string)
func (x *NodeState_Appnum4Appapi3Sysapi6) SelectFromPrev(eui string) (error, bool)  { // later this will be SelectByEui(eui string)
  //x.Id = id
  x.Eui.Load(eui)
  prevTable := x.GetPrevTableName()
  selectString := x.SelectString(prevTable) // we could pass eui into select for a nicer abstraction
  fmt.Printf("table.SelectString(): %s\n", selectString)
  results, err := x.db.Query(selectString)
  if err != nil {
    return err, false
  }
  defer results.Close()
  for results.Next() {
    var row PrevRow
    err = results.Scan( &row.Eui, &row.ValidOnboard, &row.CreatedAt, &row.UpdatedAt, &row.AppNum, &row.AppApi, &row.SysApi, &row.AppFwVersion, &row.SysFwVersion, &row.ComApi, &row.ComFwVersion, &row.LastPktRcvd, &row.LastPktRcvdTs, &row.LastVoltageRcvd, &row.LastVoltageRcvdTs, &row.LastResetCause, &row.LastResetCauseTs, &row.LastRssi, &row.LastRssiTs, &row.LastSnr, &row.LastSnrTs, &row.GlobalDownstreamEnable, &row.SystemDownstreamEnable, &row.AppDownstreamEnable, &row.ActualStateA, &row.ActualStateB, &row.ExpectedStateA, &row.ExpectedStateB,)
    if err != nil {
       return err, false
    }
    x.Eui.Load(row.Eui)
    x.ValidOnboard.Load(row.ValidOnboard)
    x.CreatedAt.Load(row.CreatedAt)
    x.UpdatedAt.Load(row.UpdatedAt)
    x.AppNum.Load(row.AppNum)
    x.AppApi.Load(row.AppApi)
    x.SysApi.Load(row.SysApi)
    x.AppFwVersion.Load(row.AppFwVersion)
    x.SysFwVersion.Load(row.SysFwVersion)
    x.ComApi.Load(row.ComApi)
    x.ComFwVersion.Load(row.ComFwVersion)
    x.LastPktRcvd.Load(row.LastPktRcvd)
    x.LastPktRcvdTs.Load(row.LastPktRcvdTs)
    x.LastVoltageRcvd.Load(row.LastVoltageRcvd)
    x.LastVoltageRcvdTs.Load(row.LastVoltageRcvdTs)
    x.LastResetCause.Load(row.LastResetCause)
    x.LastResetCauseTs.Load(row.LastResetCauseTs)
    x.LastRssi.Load(row.LastRssi)
    x.LastRssiTs.Load(row.LastRssiTs)
    x.LastSnr.Load(row.LastSnr)
    x.LastSnrTs.Load(row.LastSnrTs)
    x.GlobalDownstreamEnable.Load(row.GlobalDownstreamEnable)
    x.SystemDownstreamEnable.Load(row.SystemDownstreamEnable)
    x.AppDownstreamEnable.Load(row.AppDownstreamEnable)
    x.ActualStateA.Load(row.ActualStateA)
    x.ActualStateB.Load(row.ActualStateB)
    x.ExpectedStateA.Load(row.ExpectedStateA)
    x.ExpectedStateB.Load(row.ExpectedStateB)
    return nil, true
  }
  return nil, false
}

func (x *NodeState_Appnum4Appapi3Sysapi6) CreateTableString() string {
  return "create table " + x.GetCurrentTableName() +
  " ( " + x.Eui.String() + " " + x.Eui.Type() + " " + x.Eui.Modifiers() +
  ", " + x.ValidOnboard.String() + " " + x.ValidOnboard.Type() + " " + x.ValidOnboard.Modifiers() +
  ", " + x.CreatedAt.String() + " " + x.CreatedAt.Type() + " " + x.CreatedAt.Modifiers() +
  ", " + x.UpdatedAt.String() + " " + x.UpdatedAt.Type() + " " + x.UpdatedAt.Modifiers() +
  ", " + x.AppNum.String() + " " + x.AppNum.Type() + " " + x.AppNum.Modifiers() +
  ", " + x.AppApi.String() + " " + x.AppApi.Type() + " " + x.AppApi.Modifiers() +
  ", " + x.SysApi.String() + " " + x.SysApi.Type() + " " + x.SysApi.Modifiers() +
  ", " + x.AppFwVersion.String() + " " + x.AppFwVersion.Type() + " " + x.AppFwVersion.Modifiers() +
  ", " + x.SysFwVersion.String() + " " + x.SysFwVersion.Type() + " " + x.SysFwVersion.Modifiers() +
  ", " + x.ComApi.String() + " " + x.ComApi.Type() + " " + x.ComApi.Modifiers() +
  ", " + x.ComFwVersion.String() + " " + x.ComFwVersion.Type() + " " + x.ComFwVersion.Modifiers() +
  ", " + x.LastPktRcvd.String() + " " + x.LastPktRcvd.Type() + " " + x.LastPktRcvd.Modifiers() +
  ", " + x.LastPktRcvdTs.String() + " " + x.LastPktRcvdTs.Type() + " " + x.LastPktRcvdTs.Modifiers() +
  ", " + x.LastVoltageRcvd.String() + " " + x.LastVoltageRcvd.Type() + " " + x.LastVoltageRcvd.Modifiers() +
  ", " + x.LastVoltageRcvdTs.String() + " " + x.LastVoltageRcvdTs.Type() + " " + x.LastVoltageRcvdTs.Modifiers() +
  ", " + x.LastResetCause.String() + " " + x.LastResetCause.Type() + " " + x.LastResetCause.Modifiers() +
  ", " + x.LastResetCauseTs.String() + " " + x.LastResetCauseTs.Type() + " " + x.LastResetCauseTs.Modifiers() +
  ", " + x.LastRssi.String() + " " + x.LastRssi.Type() + " " + x.LastRssi.Modifiers() +
  ", " + x.LastRssiTs.String() + " " + x.LastRssiTs.Type() + " " + x.LastRssiTs.Modifiers() +
  ", " + x.LastSnr.String() + " " + x.LastSnr.Type() + " " + x.LastSnr.Modifiers() +
  ", " + x.LastSnrTs.String() + " " + x.LastSnrTs.Type() + " " + x.LastSnrTs.Modifiers() +
  ", " + x.GlobalDownstreamEnable.String() + " " + x.GlobalDownstreamEnable.Type() + " " + x.GlobalDownstreamEnable.Modifiers() +
  ", " + x.SystemDownstreamEnable.String() + " " + x.SystemDownstreamEnable.Type() + " " + x.SystemDownstreamEnable.Modifiers() +
  ", " + x.AppDownstreamEnable.String() + " " + x.AppDownstreamEnable.Type() + " " + x.AppDownstreamEnable.Modifiers() +
  ", " + x.ActualStateA.String() + " " + x.ActualStateA.Type() + " " + x.ActualStateA.Modifiers() +
  ", " + x.ActualStateB.String() + " " + x.ActualStateB.Type() + " " + x.ActualStateB.Modifiers() +
  ", " + x.ExpectedStateA.String() + " " + x.ExpectedStateA.Type() + " " + x.ExpectedStateA.Modifiers() +
  ", " + x.ExpectedStateB.String() + " " + x.ExpectedStateB.Type() + " " + x.ExpectedStateB.Modifiers() +
  ", " + x.constraints + " );"
}

// used internally for creating table name with current or previous version
func tableName(name string, version int) string {
   return name + "_v" + strconv.Itoa(version)
}

func newDeployment(current_version int, previous_version int) bool {
  if current_version == (previous_version + 1) {
    return true;
  }
  return false;
}

func (x *NodeState_Appnum4Appapi3Sysapi6) GetCurrentTableName() string {
  return tableName(x.tableName, x.version)
}

func (x *NodeState_Appnum4Appapi3Sysapi6) SelectString(tableName string) string {
  return "select * from " + tableName + " where eui = " + x.Eui.QueryValue() + ";"
}

// insert is only done to the current table
func (x *NodeState_Appnum4Appapi3Sysapi6) InsertString() string {
  return "insert into " + x.GetCurrentTableName() +
   " (" + x.Eui.String() + " , " +  x.ValidOnboard.String() + " , " +  x.AppNum.String() + " , " +  x.AppApi.String() + " , " +  x.SysApi.String() + " , " +  x.AppFwVersion.String() + " , " +  x.SysFwVersion.String() + " , " +  x.ComApi.String() + " , " +  x.ComFwVersion.String() + " , " +  x.LastPktRcvd.String() + " , " +  x.LastPktRcvdTs.String() + " , " +  x.LastVoltageRcvd.String() + " , " +  x.LastVoltageRcvdTs.String() + " , " +  x.LastResetCause.String() + " , " +  x.LastResetCauseTs.String() + " , " +  x.LastRssi.String() + " , " +  x.LastRssiTs.String() + " , " +  x.LastSnr.String() + " , " +  x.LastSnrTs.String() + " , " +  x.GlobalDownstreamEnable.String() + " , " +  x.SystemDownstreamEnable.String() + " , " +  x.AppDownstreamEnable.String() + " , " +  x.ActualStateA.String() + " , " +  x.ActualStateB.String() + " , " +  x.ExpectedStateA.String() + " , " +  x.ExpectedStateB.String() + ") values" +
   " (?"+ " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + ");"
}

// function returns true if at least one field has been updated
func (x *NodeState_Appnum4Appapi3Sysapi6) UpdateString() (string, bool) {
  count := 0
  first := true
  return_str := "update " + x.GetCurrentTableName() + " set "
  if x.ValidOnboard.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.ValidOnboard.String() + " = ValidOnboard.QueryValue() + "
    first = false
  }
  if x.AppNum.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.AppNum.String() + " = AppNum.QueryValue() + "
    first = false
  }
  if x.AppApi.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.AppApi.String() + " = AppApi.QueryValue() + "
    first = false
  }
  if x.SysApi.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.SysApi.String() + " = SysApi.QueryValue() + "
    first = false
  }
  if x.AppFwVersion.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.AppFwVersion.String() + " = AppFwVersion.QueryValue() + "
    first = false
  }
  if x.SysFwVersion.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.SysFwVersion.String() + " = SysFwVersion.QueryValue() + "
    first = false
  }
  if x.ComApi.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.ComApi.String() + " = ComApi.QueryValue() + "
    first = false
  }
  if x.ComFwVersion.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.ComFwVersion.String() + " = ComFwVersion.QueryValue() + "
    first = false
  }
  if x.LastPktRcvd.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastPktRcvd.String() + " = LastPktRcvd.QueryValue() + "
    first = false
  }
  if x.LastPktRcvdTs.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastPktRcvdTs.String() + " = LastPktRcvdTs.QueryValue() + "
    first = false
  }
  if x.LastVoltageRcvd.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastVoltageRcvd.String() + " = LastVoltageRcvd.QueryValue() + "
    first = false
  }
  if x.LastVoltageRcvdTs.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastVoltageRcvdTs.String() + " = LastVoltageRcvdTs.QueryValue() + "
    first = false
  }
  if x.LastResetCause.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastResetCause.String() + " = LastResetCause.QueryValue() + "
    first = false
  }
  if x.LastResetCauseTs.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastResetCauseTs.String() + " = LastResetCauseTs.QueryValue() + "
    first = false
  }
  if x.LastRssi.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastRssi.String() + " = LastRssi.QueryValue() + "
    first = false
  }
  if x.LastRssiTs.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastRssiTs.String() + " = LastRssiTs.QueryValue() + "
    first = false
  }
  if x.LastSnr.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastSnr.String() + " = LastSnr.QueryValue() + "
    first = false
  }
  if x.LastSnrTs.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastSnrTs.String() + " = LastSnrTs.QueryValue() + "
    first = false
  }
  if x.GlobalDownstreamEnable.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.GlobalDownstreamEnable.String() + " = GlobalDownstreamEnable.QueryValue() + "
    first = false
  }
  if x.SystemDownstreamEnable.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.SystemDownstreamEnable.String() + " = SystemDownstreamEnable.QueryValue() + "
    first = false
  }
  if x.AppDownstreamEnable.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.AppDownstreamEnable.String() + " = AppDownstreamEnable.QueryValue() + "
    first = false
  }
  if x.ActualStateA.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.ActualStateA.String() + " = ActualStateA.QueryValue() + "
    first = false
  }
  if x.ActualStateB.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.ActualStateB.String() + " = ActualStateB.QueryValue() + "
    first = false
  }
  if x.ExpectedStateA.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.ExpectedStateA.String() + " = ExpectedStateA.QueryValue() + "
    first = false
  }
  if x.ExpectedStateB.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.ExpectedStateB.String() + " = ExpectedStateB.QueryValue() + "
    first = false
  }
  return_str += " where eui = " + x.Eui.QueryValue() + ";"
  if count > 0 {
    return return_str, true
  } else {
    return "", false
  }
}

func (x *NodeState_Appnum4Appapi3Sysapi6) DeleteString(tableName string) string {
  return "delete from " + tableName + " where eui = " + x.Eui.QueryValue() + ";"
}

