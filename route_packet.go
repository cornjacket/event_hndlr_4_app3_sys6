package main

import (
	"fmt"
	"net/http"
	//"time"
	"encoding/json"
	"github.com/cornjacket/iot/message"
	nodestate "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6"
	packet "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/packet_appnum4appapi3sysapi6"
)

// TODO: Need to add a data layer so that I can injest into a raw table.
// TODO: Hand the packet over to the system for processing and then hand over to the application to handle
func newUpPacketJson(w http.ResponseWriter, r *http.Request) {

	returnCode := 201
	fmt.Println("newUpPacketJson")
	// Stub an example packet to be populated from the body
	//p := data.Packet{}
        p := message.UpPacket{}

	// Populate the packet data
	err := json.NewDecoder(r.Body).Decode(&p)
	if err != nil {
		returnCode = 400
	}

        //p.CreatedAt = time.Now() // not needed if the db layer handles for me - no create_at field - need to have a db structure
	fmt.Printf("newUpPacketJson before insert: eui: %s, packet: %s, raw: %v\n", p.Eui, p.Data, p)

	pkt := packet.New()
	var pktRow packet.Row
	pktRow.Ts =	int64(p.Ts)
	pktRow.Dr =	p.Dr
	pktRow.Eui =	p.Eui
	if p.Ack {
		pktRow.Ack = 1
	}
	pktRow.Cmd =	p.Cmd
	pktRow.Snr =	int(p.Snr*10)
	pktRow.Data =	p.Data
	pktRow.Fcnt =	p.Fcnt
	pktRow.Freq =	int64(p.Freq)
	pktRow.Port =	p.Port
	pktRow.Rssi =	p.Rssi
	err = pkt.Insert(&pktRow)
	if err != nil {
		fmt.Printf("pkt.Insert failed. error: %v\n", err)
	} else {
		fmt.Printf("pkt.Insert success. Eui: %s\n", p.Eui)
	}

	// Execute lookup based on EUI to get Class and Location
	//appName, location := getAppNamePostLocation(p.Eui)
	//p.Location = "0.0" + "." + location

	//debugOut(p, appName) // this could be made into method
	//p.Create(appName)
	// Marshal provided interface into JSON strucutre

	// TODO: The following is for test/illustrative purposes. An Eui based query should happen first
	nodeState := nodestate.New()
	currentTable := nodeState.GetCurrentTableName()
	fmt.Printf("currentTable: %s, CreateTableString: %s\n", currentTable, nodeState.CreateTableString())
	// nodeRow was needed because there use to be an nodeState.Insert
	var nodeRow nodestate.Row

	nodeRow.Eui           = p.Eui 
	nodeRow.ValidOnboard  = 1
	//row.SysApi        = 1
	//row.AppApi        = 1

	nodeState.GetByEui(nodeRow.Eui)

	pj, _ := json.Marshal(p)

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(returnCode)
	fmt.Fprintf(w, "%s", pj)
}

/*
import (
  // Standard library packets
  "fmt"
  "os"
  "database/sql"
  _ "github.com/go-sql-driver/mysql"
)

func main() {

  dbPassword := os.Getenv("DB_PASSWORD")
  if dbPassword == "" {
    fmt.Printf("MAIN\tdb Password not provided. Exiting...\n")
    os.Exit(0)
  }

  fmt.Println("Go MySQL Tutorial")

  //var db driver.Conn
  var db *sql.DB
  var err error
  //db, err := sql.Open("mysql", "root:"+dbPassword+"@tcp(127.0.0.1:3306)/test?parseTime=true")
  db, err = sql.Open("mysql", "root:"+dbPassword+"@tcp(127.0.0.1:3306)/test?parseTime=true")

  if err != nil {
    panic(err.Error())
  }

  defer db.Close()


}
*/
