package fcnt

import (
  "strconv"
)

type Fcnt struct {
  value int
  updated bool
}

func New() *Fcnt {
  return &Fcnt{}
}

func (x *Fcnt) IsUpdated() bool {
  return x.updated
}

func (x *Fcnt) Get() int {
  return x.value
}

func (x *Fcnt) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *Fcnt) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *Fcnt) String() string {
  return "Fcnt"
}

func (x *Fcnt) Type() string {
  return "int"
}

func (x *Fcnt) Modifiers() string {
  return "not null"
}

func (x *Fcnt) QueryValue() string {
  return strconv.Itoa(x.value)
}

