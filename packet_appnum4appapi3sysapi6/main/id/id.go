package id

import (
  "strconv"
)

type Id struct {
  value int64
  updated bool
}

func New() *Id {
  return &Id{}
}

func (x *Id) IsUpdated() bool {
  return x.updated
}

func (x *Id) Get() int64 {
  return x.value
}

func (x *Id) Set(value int64) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *Id) Load(value int64) {
  x.value = value
  x.updated = false
}

func (x *Id) String() string {
  return "Id"
}

func (x *Id) Type() string {
  return "bigint"
}

func (x *Id) Modifiers() string {
  return "not null auto_increment"
}

func (x *Id) QueryValue() string {
  return strconv.FormatInt(x.value, 10)
}

