package main

import (
	"net/http"
	"os"
	"time"
	"fmt"
	"database/sql"
	nodestate "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/nodestate_appnum4appapi3sysapi6"
	packet "github.com/cornjacket/event_hndlr_appnum4_appapi3_sysapi6/packet_appnum4appapi3sysapi6"
)

func main() {

	checkIfProjectNeedsBuild()
	setupTablePackages()

	p("event_hndlr", version(), "started at", config.Address)

	// handle static assets
	mux := http.NewServeMux()
	files := http.FileServer(http.Dir(config.Static))
	mux.Handle("/static/", http.StripPrefix("/static/", files))

	//
	// all route patterns matched here
	// route handler functions defined in other files
	//

	// index
	mux.HandleFunc("/", index)
	// error
	mux.HandleFunc("/err", err)

	// data
	mux.HandleFunc("/packet", newUpPacketJson)


	// starting up the server
	server := &http.Server{
		Addr:           config.Address,
		Handler:        mux,
		ReadTimeout:    time.Duration(config.ReadTimeout * int64(time.Second)),
		WriteTimeout:   time.Duration(config.WriteTimeout * int64(time.Second)),
		MaxHeaderBytes: 1 << 20,
	}
	if err := server.ListenAndServe(); err != nil {
		fmt.Println(err.Error())
	}
}

func init() {
    fmt.Println("main init() called.\n")
    }

func checkIfProjectNeedsBuild() {
  if _, err := os.Stat("./build.json"); err == nil {
    fmt.Println("Project config has changed but build was not invoked. Run \"guild Build\"\n")
    os.Exit(1)
   }
}

func setupTablePackages() {
  dbPassword := os.Getenv("DB_PASSWORD")
  if dbPassword == "" {
    fmt.Printf("init\tdb Password not provided. Exiting...\n")
    os.Exit(0)
  }
  db_conn, err := sql.Open("mysql", "root:"+dbPassword+"@tcp(127.0.0.1:3306)/test?parseTime=true")

  if err != nil {
    panic(err.Error())
  }
  nodestate.Init(db_conn)
  packet.Init(db_conn)
}
